package com.skyids;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkyidsQingkePushApplication {
	public static void main(String[] args) {
		SpringApplication.run(SkyidsQingkePushApplication.class, args);
	}
}
