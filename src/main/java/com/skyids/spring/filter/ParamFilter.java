package com.skyids.spring.filter;


import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * User:wangkaijin
 * Date: 2017/6/29
 */
@Order(9)
@WebFilter(filterName = "ParamFilter", urlPatterns = "/sms/*")
public class ParamFilter implements Filter {
    private final static String URLENSTR = "v1/api";
    //url上参数的key值
    private static final String SING = "sign";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        //备份request
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        Map<String, String[]> parameterMap = httpRequest.getParameterMap();
        String sign = httpRequest.getParameter(SING);

        Map<String, String> keyMap = new HashMap<>();
        for(Map.Entry<String, String[]> entry :parameterMap.entrySet()){
            if(!entry.getKey().equals(SING)){
                keyMap.put(entry.getKey(),entry.getValue()[0]);
            }
        }
        boolean flag = true;
//        try {
//            flag = HttpCrypto.decrypt(keyMap, sign);
//        } catch (Exception e) {
//            response.sendError(403005);
//            return;
//        }
        if (flag) {
            filterChain.doFilter(httpRequest, servletResponse);
        } else {
            response.sendError(403005);
        }

    }

    @Override
    public void destroy() {
    }
}
