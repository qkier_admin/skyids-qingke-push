package com.skyids.web.push.controller;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.http.MethodType;
import com.google.gson.Gson;
import com.skyids.core.constant.SmsConst;
import com.skyids.core.constant.SmsConstAli;
import com.skyids.core.constant.SysConstAli;
import com.skyids.core.constant.SysConst;
import com.skyids.core.data.AliSmsProps;
import com.skyids.core.data.ZYSmsProps;
import com.skyids.core.dto.RespMsg;
import com.skyids.core.dto.RespMsgKit;
import com.skyids.core.enums.SMSError;
import com.skyids.core.enums.SystemResCode;
import com.skyids.core.msg.AliSMSClient;
import com.skyids.core.msg.ZYSMSClient;
import com.skyids.core.msg.request.BaseRequest;
import com.skyids.core.msg.request.BatchSendSmsRequest;
import com.skyids.core.msg.request.SingleSendSmsRequest;
import com.skyids.core.msg.response.BaseResponse;
import com.skyids.core.msg.response.BatchSendSmsResponse;
import com.skyids.core.msg.response.SingleSendSmsResponse;
import com.skyids.core.util.MD5Util;
import com.skyids.core.util.StringUtil;
import com.skyids.web.push.entity.SendMessage;
import com.skyids.web.push.entity.SmsReqParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * 提供适配轻客短信接口
 * User:wangkaijin
 * Date: 2017/9/7
 */
@RestController
@RequestMapping("/sms")
public class PushController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PushController.class);

    @Autowired
    private ZYSmsProps zySmsProps;

    @Autowired
    private AliSmsProps aliSmsProps;

    @PostMapping("/send-by-tpl")
    public RespMsg newSendCode(@RequestBody SendMessage sendMessage)
    {
        RespMsg respMsg;
        SmsReqParam param = new SmsReqParam();
        param.setParams(sendMessage.getArgs());
        param.setPlatform(SysConst.PLATFORM_ID);
        param.setSignId(SysConst.QINGKE_SIGN);
        param.setTemplateId(SysConst.TPL_MAP.get(sendMessage.getTpl_id()));
        param.setTargets(Arrays.asList(sendMessage.getMobile()));
        AliSMSClient alismsClient=new AliSMSClient();

        //解析电话号码清单
        if(sendMessage.getMobile() !=null && sendMessage.getMobile().length>0) {

            StringBuffer sb = new StringBuffer();
            for (String ps : sendMessage.getMobile()
            ) {
                sb.append(ps);
                sb.append(",");
            }
            String phoneStr=sb.deleteCharAt(sb.lastIndexOf(",")).toString();

            CommonRequest request = new CommonRequest();
            request.setSysMethod(MethodType.POST);
            request.setSysDomain("dysmsapi.aliyuncs.com");
            request.setSysVersion("2017-05-25");
            request.setSysAction("SendSms");
            request.putQueryParameter("RegionId", "cn-hangzhou");
            request.putQueryParameter("PhoneNumbers", phoneStr);
            request.putQueryParameter("SignName", "轻客");
            request.putQueryParameter("TemplateCode", SysConstAli.ALI_TPL_MAP.get(sendMessage.getTpl_id()));
            LOGGER.info("===>message:" + convertMessage(sendMessage.getArgs(),sendMessage.getTpl_id()));
            request.putQueryParameter("TemplateParam", convertMessage(sendMessage.getArgs(),sendMessage.getTpl_id()));

            CommonResponse response = alismsClient.smsSendExecutor(request);
            JSONObject rs = JSONObject.parseObject(response.getData());
            LOGGER.info("===>阿里云短信发送结果:" + rs.toJSONString());
            if (SmsConstAli.ALI_SUCCESS.equals(rs.getString("Code"))) {
                //发送成功
                respMsg = new RespMsg(SystemResCode.STATUS_SUCCESS.getCode(), SystemResCode.STATUS_SUCCESS.getMsg(), rs);
            } else {
                respMsg = RespMsgKit.buildFailedRespMsg(rs.toJSONString());
            }
        }
        else
        {
            respMsg = RespMsgKit.buildFailedRespMsg("电话号码为空");
        }
        return respMsg;
    }

    private String convertMessage(LinkedHashMap<String, String> args,Integer type)
    {
        //短信内容转化
        String code="";
        String washer_id="";
        String contact_mobile="";
        StringBuilder out=new StringBuilder();
        out.append("{");
        for(String key : args.keySet())
        {
            switch(key.toLowerCase())
            {
                case "validatecode":
                    code=args.get(key);
                    out.append(String.format("\"code\":\"%s\"",code));
                    break;
                case "washerid":
                    washer_id=args.get(key);
                    out.append(String.format("\"washerid\":\"%s\"",washer_id));
                    break;
                case "contact_mobile":
                    contact_mobile=args.get(key);
                    out.append(String.format("\"contact_mobile\":\"%s\"",contact_mobile));
                    break;
            }
        }
        out.append("}");
        return out.toString();
    }

    @PostMapping("/send-by-tpl1")
    public RespMsg sendCode(@RequestBody SendMessage sendMessage) {
        RespMsg respMsg;
        SmsReqParam param = new SmsReqParam();
        param.setParams(sendMessage.getArgs());
        param.setPlatform(SysConst.PLATFORM_ID);
        param.setSignId(SysConst.QINGKE_SIGN);
        param.setTemplateId(SysConst.TPL_MAP.get(sendMessage.getTpl_id()));
        param.setTargets(Arrays.asList(sendMessage.getMobile()));
        ZYSMSClient zysmsClient = new ZYSMSClient(zySmsProps);
        long timestamp = System.currentTimeMillis();
        String sign = MD5Util.getMD5(zySmsProps.getApiAccount() + zySmsProps.getApiKey() + timestamp);
        BaseRequest request;
        if (!StringUtil.isEmpty(param.getTarget())) {
            //推送单个人
            //构造发送请求
            SingleSendSmsRequest singleRequest = new SingleSendSmsRequest();
            singleRequest.setMobile(param.getTarget());
            singleRequest.setSingerId(StringUtil.isEmpty(param.getSignId()) ? zySmsProps.getDefaultSign() : param.getSignId());
            singleRequest.setTemplateId(param.getTemplateId());
            request = singleRequest;
        } else if (param.getTargets() != null && param.getTargets().size() > 0) {
            //群发
            BatchSendSmsRequest batchSendSmsRequest = new BatchSendSmsRequest();
            batchSendSmsRequest.setMobile(param.getTargets());
            batchSendSmsRequest.setSingerId(StringUtil.isEmpty(param.getSignId()) ? zySmsProps.getDefaultSign() : param.getSignId());
            batchSendSmsRequest.setTemplateId(param.getTemplateId());
            request = batchSendSmsRequest;
        } else {
            respMsg = RespMsgKit.buildRespMsg(SMSError.MISS_PARAM.getCode(), SMSError.MISS_PARAM.getMessage());
            return respMsg;
        }
        request.setApiAccount(zySmsProps.getApiAccount());
        request.setAppId(zySmsProps.getAppids().get(param.getPlatform()));
        request.setSign(sign);
        request.setTimeStamp(timestamp);
        //解析传入的参数
        if (param.getParams() != null && param.getParams().size() > 0) {
            LinkedHashMap<String, String> params = param.getParams();
            Iterator<String> valueIterator = params.values().iterator();
            StringBuffer sb = new StringBuffer();
            while (valueIterator.hasNext()) {
                sb.append(valueIterator.next());
                sb.append(",");
            }
            String paramStr = sb.deleteCharAt(sb.lastIndexOf(",")).toString();
            request.setParam(paramStr);
        }
        //确定使用 默认签名和模板 / 传入的签名和模板
        BaseResponse response = zysmsClient.smsSendExecutor(request);
        LOGGER.info("===>智语短信发送结果:" + new Gson().toJson(response));
        if (response.getStatus() == SmsConst.ZHIYU_SUCCESS) {
            //发送成功
            if (response instanceof SingleSendSmsResponse) {
                SingleSendSmsResponse singleResponse = (SingleSendSmsResponse) response;
                respMsg = new RespMsg(SystemResCode.STATUS_SUCCESS.getCode(), SystemResCode.STATUS_SUCCESS.getMsg(), singleResponse.getSmsId());
            } else {
                BatchSendSmsResponse batchResponse = (BatchSendSmsResponse) response;
                respMsg = new RespMsg(SystemResCode.STATUS_SUCCESS.getCode(), SystemResCode.STATUS_SUCCESS.getMsg(), batchResponse.getSmsId());
            }
        } else {
            respMsg = RespMsgKit.buildFailedRespMsg(response.getDesc());
        }
        return respMsg;
    }
}
