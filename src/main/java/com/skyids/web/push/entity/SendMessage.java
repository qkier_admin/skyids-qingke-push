package com.skyids.web.push.entity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 轻客请求model
 * User:wangkaijin
 * Date: 2017/9/7
 */

public class SendMessage {
    private Integer tpl_id; //模板编号
    private String[] mobile; //发送手机列表
    private LinkedHashMap<String, String> args; //参数

    public Integer getTpl_id() {
        return tpl_id;
    }

    public void setTpl_id(Integer tpl_id) {
        this.tpl_id = tpl_id;
    }

    public String[] getMobile() {
        return mobile;
    }

    public void setMobile(String[] mobile) {
        this.mobile = mobile;
    }

    public LinkedHashMap<String, String> getArgs() {
        return args;
    }

    public void setArgs(LinkedHashMap<String, String> args) {
        this.args = args;
    }
}
