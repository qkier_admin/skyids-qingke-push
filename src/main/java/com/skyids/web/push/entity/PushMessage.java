package com.skyids.web.push.entity;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/27
 * Time: 11:24
 */
public class PushMessage {

    private Long id;                    //消息id

    private Long creatorId;             //消息创建者 PLATFORM_ID
    private Long platformId;            //不同平台的 APP 所用的appKey 和 appSecret 都不同，需要明确指定 PLATFORM_ID
    private List<Long> targets;         //目标用户

    private Integer pushType;           //定义推送类型，推送 消息 or 通知2
    private Integer priority;           //消息优先级 9
    private String msgType;             //消息类型 9999
    private Integer ttl;                //消息保存时间，如果终端不在线，当在这个时间内终端一旦上线，消息都会推过去，超过时间将不会再进行主动推送
    private String title;               //消息标题
    private String body;                //消息内容
    private String params;              //业务参数
    private String deviceId;            //设备ID

    public PushMessage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public List<Long> getTargets() {
        return targets;
    }

    public void setTargets(List<Long> targets) {
        this.targets = targets;
    }

    public Integer getPushType() {
        return pushType;
    }

    public void setPushType(Integer pushType) {
        this.pushType = pushType;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Integer getTtl() {
        return ttl;
    }

    public void setTtl(Integer ttl) {
        this.ttl = ttl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
