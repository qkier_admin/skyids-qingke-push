package com.skyids.web.push.entity;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/14
 * Time: 13:40
 */
public class SmsSendVO {

    private Long platform;           //必选，请求来源平台，现在都设置为1

    private List<String> target;        //必选，用户手机号

    private String signId;              //可选，短信签名id，可选，默认为【创维群欣】

    private String templateId;          //必选，短信模版id

    private Map<String, String> params;  //可选，模版中有变量的话为必选，变量的值key value形式

    private Integer type;               //必选，短信类型 1=短信验证码；2=模版短信；3=推广短信

    private Integer channel;            //必选，发送渠道，1=阿里云；2=智语

    public SmsSendVO() {
    }

    public Long getPlatform() {
        return platform;
    }

    public void setPlatform(Long platform) {
        this.platform = platform;
    }

    public List<String> getTarget() {
        return target;
    }

    public void setTarget(List<String> target) {
        this.target = target;
    }

    public String getSignId() {
        return signId;
    }

    public void setSignId(String signId) {
        this.signId = signId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }
}
