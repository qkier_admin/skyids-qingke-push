package com.skyids.web.push.entity;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/29
 * Time: 14:15
 */
public class SmsReqParam {

    private Long platform;              //必选，请求来源平台id，目前云锁平台platformId 为 56799706774511616

    private String target;              //用户手机号，用于单发场景

    private List<String> targets;       //可选，用户手机号集合，用于群发场景

    private String signId;              //可选，短信签名id，可选，默认为【创维群欣】

    private String templateId;          //可选，短信模版id，发送通知时必须指明模板

    private String code;                //短信验证码

    private LinkedHashMap<String,String> params;  //可选，发送通知时为必选，构造规则参见推送平台文档

    public Long getPlatform() {
        return platform;
    }

    public void setPlatform(Long platform) {
        this.platform = platform;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }

    public String getSignId() {
        return signId;
    }

    public void setSignId(String signId) {
        this.signId = signId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public LinkedHashMap<String, String> getParams() {
        return params;
    }

    public void setParams(LinkedHashMap<String, String> params) {
        this.params = params;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
