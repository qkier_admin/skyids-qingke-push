package com.skyids.core.enums;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/29
 * Time: 14:39
 */
public enum SMSError {

    MISS_PARAM(10001, "参数错误");


    private Integer code;
    private String message;

    SMSError(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
