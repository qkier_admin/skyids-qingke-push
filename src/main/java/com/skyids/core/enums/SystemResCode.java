package com.skyids.core.enums;

/**
 * 全局返回码
 * User:wangkaijin
 * Date: 2017/6/18
 */

public enum SystemResCode {
    STATUS_SUCCESS(0, "操作成功"),
    STATUS_FAILED(503001, "服务器内部错误"),
    STATUS_WARNING(-2, "发生异常"),
    PLATFORM_INVOKE(-3, "平台调用异常");

    private Integer code;
    private String msg;

    SystemResCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
