package com.skyids.core.data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/12
 * Time: 16:29
 */
@Component
@ConfigurationProperties(prefix = "zysms")
public class ZYSmsProps {

    private String apiAccount;

    private String apiKey;

    private String defaultSign;

    private String defaultTemplate;

    private String baseUrl;

    private String singleSend;

    private String batchSend;

    private Map<Long,String> appids;

    public ZYSmsProps() {
    }

    public String getApiAccount() {
        return apiAccount;
    }

    public void setApiAccount(String apiAccount) {
        this.apiAccount = apiAccount;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getSingleSend() {
        return singleSend;
    }

    public void setSingleSend(String singleSend) {
        this.singleSend = singleSend;
    }

    public String getBatchSend() {
        return batchSend;
    }

    public void setBatchSend(String batchSend) {
        this.batchSend = batchSend;
    }

    public Map<Long, String> getAppids() {
        return appids;
    }

    public void setAppids(Map<Long, String> appids) {
        this.appids = appids;
    }

    public String getDefaultSign() {
        return defaultSign;
    }

    public void setDefaultSign(String defaultSign) {
        this.defaultSign = defaultSign;
    }

    public String getDefaultTemplate() {
        return defaultTemplate;
    }

    public void setDefaultTemplate(String defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
    }
}
