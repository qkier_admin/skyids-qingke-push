package com.skyids.core.msg;

import com.google.gson.Gson;
import com.skyids.core.util.StringUtil;
import com.skyids.core.msg.request.BaseRequest;
import com.skyids.core.msg.request.BatchSendSmsRequest;
import com.skyids.core.msg.request.SingleSendSmsRequest;
import com.skyids.core.msg.response.BaseResponse;
import com.skyids.core.msg.response.BatchSendSmsResponse;
import com.skyids.core.msg.response.SingleSendSmsResponse;
import com.skyids.core.data.ZYSmsProps;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/12
 * Time: 16:26
 */
public class ZYSMSClient implements SMSDefaultClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZYSMSClient.class);

    private ZYSmsProps zySmsProps;

    public ZYSMSClient(ZYSmsProps zySmsProps) {
        this.zySmsProps = zySmsProps;
    }

    @Override
    public BaseResponse smsSendExecutor(BaseRequest request) {

        Gson gson = new Gson();
        String url;
        if (request instanceof SingleSendSmsRequest){
            //发送单条短信
            SingleSendSmsRequest singleRequest = (SingleSendSmsRequest)request;
            String paramStr = gson.toJson(singleRequest);
            LOGGER.info("paramStr===>"+paramStr);
            url = zySmsProps.getBaseUrl() + request.getAppId() + zySmsProps.getSingleSend();
            try {
                String response = execute(url, paramStr);
                if (!StringUtil.isEmpty(response)){
                    //将返回值序列化为response对象
                    return gson.fromJson(response, SingleSendSmsResponse.class);
                }else {
                    return null;
                }
            } catch (Exception e) {
                LOGGER.error("解析短信发送请求智语接口返回值报错！",e);
                e.printStackTrace();
                return null;
            }

        }else if (request instanceof BatchSendSmsRequest){
            //群发短信
            BatchSendSmsRequest batchRequest = (BatchSendSmsRequest)request;
            url = zySmsProps.getBaseUrl() + request.getAppId() + zySmsProps.getBatchSend();
            //处理请求参数
            String mobileStr = gson.toJson(batchRequest.getMobile());
            mobileStr = mobileStr.substring(mobileStr.indexOf("[")+1, mobileStr.indexOf("]")).replace("\"","");

            String temp = gson.toJson(request);
            String front = temp.substring(0, temp.indexOf("["));
            String back = temp.substring(temp.indexOf("]")+1);
            String paramStr = new StringBuffer().append(front).append("\"").append(mobileStr).append("\"").append(back).toString();

            LOGGER.info("paramStr===>"+paramStr);

            try {
                String response = execute(url, paramStr);
                if (!StringUtil.isEmpty(response)){
                    //将返回值序列化为response对象
                    return gson.fromJson(response, BatchSendSmsResponse.class);
                }else {
                    return null;
                }
            } catch (Exception e) {
                LOGGER.error("解析短信发送请求智语接口返回值报错！",e);
                e.printStackTrace();
                return null;
            }

        }else {
            LOGGER.error("短信发送请求构造错误！");
            return null;
        }

    }


    private String execute(String url,String param) {

        //构造http客户端
        CloseableHttpClient client = HttpClients.createDefault();

        //构造请求，设置头信息，和请求体参数
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type","application/json; charset=UTF-8");
        StringEntity entity = new StringEntity(param, "utf-8");
        httpPost.setEntity(entity);

        //执行请求
        CloseableHttpResponse response = null;
        try {
            response = client.execute(httpPost);

            if (response != null){
                return EntityUtils.toString(response.getEntity(),"utf-8");
            }else {
                return null;
            }
        } catch (IOException e) {
            LOGGER.error("发送请求智语接口报错！",e);
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (response != null)
                    response.close();
                if (client != null)
                    client.close();
            } catch (IOException e) {
                LOGGER.error("http client / response 关闭报错！",e);
                e.printStackTrace();
            }
        }

    }



}
