package com.skyids.core.msg;

import com.google.gson.Gson;
import com.skyids.core.data.AliSmsProps;
import com.skyids.core.msg.request.BaseRequest;
import com.skyids.core.msg.request.BatchSendSmsRequest;
import com.skyids.core.msg.request.SingleSendSmsRequest;
import com.skyids.core.msg.response.BaseResponse;
import com.skyids.core.msg.response.BatchSendSmsResponse;
import com.skyids.core.msg.response.SingleSendSmsResponse;
import com.skyids.core.util.StringUtil;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

/**
 * Created by IntelliJ IDEA.
 * User: qkier-tianmin
 * Date: 2020/6/18
 * Time: 09:37
 */
public class AliSMSClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZYSMSClient.class);


    public AliSMSClient() {

    }

    public CommonResponse smsSendExecutor(CommonRequest request) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4Foc1dXCUPz4eqYeKQX1", "OWzza1aAV2cmiNeb7g60AiVVazZhQA");
        IAcsClient client = new DefaultAcsClient(profile);
        try {
            LOGGER.info("===>request:" + request.toString());
            CommonResponse response = client.getCommonResponse(request);
            LOGGER.info("===>response:" + response.toString());
            //System.out.println(response.getData());
            return response;
        } catch (ServerException e) {
            LOGGER.error("解析短信发送请求Server阿里云短信返回值报错！",e);
            e.printStackTrace();
            return null;
        } catch (ClientException e) {
            LOGGER.error("解析短信发送请求Client阿里云短信返回值报错！",e);
            e.printStackTrace();
            return null;
        }
    }
}
