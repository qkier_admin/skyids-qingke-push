package com.skyids.core.msg.request;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/12
 * Time: 16:16
 */
public class BatchSendSmsRequest extends BaseRequest {

    private List<String> mobile;

    private String templateId;

    private String singerId;

    public List<String> getMobile() {
        return mobile;
    }

    public void setMobile(List<String> mobile) {
        this.mobile = mobile;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getSingerId() {
        return singerId;
    }

    public void setSingerId(String singerId) {
        this.singerId = singerId;
    }

    @Override
    public String toString() {
        return "BatchSendSmsRequest{" +
                super.toString() +
                ", mobile=" + mobile +
                ", templateId='" + templateId + '\'' +
                ", singerId='" + singerId + '\'' +
                '}';
    }
}
