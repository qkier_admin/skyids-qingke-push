package com.skyids.core.msg.request;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/12
 * Time: 16:11
 */
public class SingleSendSmsRequest extends BaseRequest {

    private String mobile;

    private String templateId;

    private String singerId;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getSingerId() {
        return singerId;
    }

    public void setSingerId(String singerId) {
        this.singerId = singerId;
    }

    @Override
    public String toString() {
        return "SingleSendSmsRequest{" +
                super.toString() +
                ", mobile='" + mobile + '\'' +
                ", templateId='" + templateId + '\'' +
                ", singerId='" + singerId + '\'' +
                '}';
    }
}
