package com.skyids.core.msg.request;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/12
 * Time: 16:03
 */
public class BaseRequest {

    private String apiAccount;

    private String appId;

    private String sign;

    private Long timeStamp;

    private String param;

    private String userData;

    private String extNumber;

    private String statusPushAddr;

    public String getApiAccount() {
        return apiAccount;
    }

    public void setApiAccount(String apiAccount) {
        this.apiAccount = apiAccount;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getUserData() {
        return userData;
    }

    public void setUserData(String userData) {
        this.userData = userData;
    }

    public String getExtNumber() {
        return extNumber;
    }

    public void setExtNumber(String extNumber) {
        this.extNumber = extNumber;
    }

    public String getStatusPushAddr() {
        return statusPushAddr;
    }

    public void setStatusPushAddr(String statusPushAddr) {
        this.statusPushAddr = statusPushAddr;
    }

    @Override
    public String toString() {
        return "apiAccount='" + apiAccount + '\'' +
                ", appId='" + appId + '\'' +
                ", sign='" + sign + '\'' +
                ", timeStamp=" + timeStamp +
                ", param='" + param + '\'' +
                ", userData='" + userData + '\'' +
                ", extNumber='" + extNumber + '\'' +
                ", statusPushAddr='" + statusPushAddr + '\'';
    }
}
