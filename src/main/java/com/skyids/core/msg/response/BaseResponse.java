package com.skyids.core.msg.response;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/12
 * Time: 16:04
 */
public class BaseResponse {

    private int status;

    private String desc;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "status=" + status +
                ", desc='" + desc + '\'' +
                '}';
    }
}
