package com.skyids.core.msg.response;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/12
 * Time: 16:22
 */
public class BatchSendSmsResponse extends BaseResponse {

    private String smsId;

    public String getSmsId() {
        return smsId;
    }

    public void setSmsId(String smsId) {
        this.smsId = smsId;
    }

    @Override
    public String toString() {
        return "BatchSendSmsResponse{" +
                super.toString() +
                ", smsId='" + smsId + '\'' +
                '}';
    }

}
