package com.skyids.core.msg;


import com.skyids.core.msg.request.BaseRequest;
import com.skyids.core.msg.response.BaseResponse;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/12
 * Time: 15:17
 */
public interface SMSDefaultClient {

    /**
     * 短信发送执行方法
     *
     * @param request
     * @return
     */
    BaseResponse smsSendExecutor(BaseRequest request);

}
