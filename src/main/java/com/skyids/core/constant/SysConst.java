package com.skyids.core.constant;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * Created by fuchuanwu on 2017/04/21.
 */
public class SysConst {

    /**业务系统id**/
    public static final Long PLATFORM_ID = 66919661010178048L;

    /**
     * 短信签名ID
     */
    public static final String QINGKE_SIGN = "msncE7u61fbq500D";

    /**
     * 暴露模板ID与真实模板ID的关系容器
     */
    public static final Map<Integer,String> TPL_MAP = new HashMap<>();

    public final static String ENCODEING_UTF8 = "UTF-8";   //编码

    static {
        //2020年3月前群欣模板映射关系
//        TPL_MAP.put(8,"mtl6s4KP6UP18sh7");
//        TPL_MAP.put(9,"mtl13n8E01Cqjs67");
//        TPL_MAP.put(10,"mtl66je6X7Q1e03x");
//        TPL_MAP.put(11,"mtlT8wCw12676QJj");
//        TPL_MAP.put(16,"mtl4i68LZZ20x2P2");
//        TPL_MAP.put(17,"mtl80364W8AgU58v");
//        TPL_MAP.put(18,"mtlJ21C64UN8tNl8");
//        TPL_MAP.put(20,"mtlfkwh7Azyi929O");
//        TPL_MAP.put(21,"mtl8d29e0Q799244");
//        TPL_MAP.put(22,"mtl0iZo8g3YsYsR0");
//        TPL_MAP.put(25,"mtlM5m2o4j9NS6rj");

        //2020年3月后轻客智能模板映射关系
        //智语短信模板
        TPL_MAP.put(8,"mtl90CQ04G20R15R");//验证码 #code#，您正在通过手机号码注册账号，如非本人操作，请忽略。
        TPL_MAP.put(9,"mtle54w0372o780q");//验证码 #code#，您正在登录账号，如非本人操作，请忽略。
        TPL_MAP.put(10,"mtl9vy9Jb5wz86U7");//验证码 #code# ，您正在重置登录密码，如非本人操作，请忽略。
        TPL_MAP.put(11,"mtlcju6555si4620");//验证码 #code# ，您正在绑定手机号，如非本人操作，请忽略。
        TPL_MAP.put(16,"mtl6UN39sINRBk5d");//尊敬的轻客用户,您使用的#washerid#洗衣机已经洗衣完毕, 请尽快取衣哦!
        TPL_MAP.put(17,"mtlbjR8zi8M73sx2");//尊敬的轻客用户,您正在使用的#washerid#洗衣机出现故障,请打开轻客APP查看故障原因和解决方案。
        TPL_MAP.put(18,"mtl278N68H190915");//尊敬的轻客用户,您锁定的#washerid#洗衣机只剩余一分钟支付时间啦,请尽快支付使用。
        TPL_MAP.put(20,"mtl15V5RDZrEfd3Z");//尊敬的轻客用户，您使用的 #washerid# 洗衣机启动失败，洗衣款项已经退款至您的轻币账户，可在下次洗衣时使用，请前往“我的钱包”查看。
        TPL_MAP.put(21,"mtl6ML9aAR152yL8");//尊敬的用户，您的洗衣机押金已退回原支付账户，请注意查收，谢谢。
        TPL_MAP.put(22,"mtl0ia96BTGGzL4H");//亲，免费洗衣券已发放到您的轻客账户，请尽快使用。感谢您反馈使用轻客洗衣遇到的问题，谢谢~
        TPL_MAP.put(25,"mtlGf7800TLO12Wl");//您正在使用的#washerid#洗衣机出现故障,请在轻客APP查看故障原因和解决方案,或联系#contact_mobile#解决。
    }

}
