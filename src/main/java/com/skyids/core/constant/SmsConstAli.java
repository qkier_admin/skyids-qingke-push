package com.skyids.core.constant;

public class SmsConstAli {

    public static final int CAPTCHA = 1;
    public static final int TEMPLATE = 2;
    public static final int  PROMOTE = 3;

    //短信发送渠道
    public static final int ALI = 1;

    public static final String ALI_SUCCESS = "OK";

    //短信类型
    public static final String ALI_SMS_TYPE_NOTICE = "8";
    public static final String ALI_SMS_TYPE_CAPTCHA = "9";
    public static final String ALI_SMS_TYPE_PROMOTE = "11";
}
