package com.skyids.core.constant;

/**
 * Created by IntelliJ IDEA.
 * User: skyworth-qx-xubo
 * Date: 2017/6/14
 * Time: 14:26
 */
public class SmsConst {

    public static final int CAPTCHA = 1;
    public static final int TEMPLATE = 2;
    public static final int  PROMOTE = 3;

    //短信发送渠道
    public static final int ZHIYU = 2;

    public static final int ZHIYU_SUCCESS = 0;

    //智语短信类型
    public static final String ZY_SMS_TYPE_NOTICE = "8";
    public static final String ZY_SMS_TYPE_CAPTCHA = "9";
    public static final String ZY_SMS_TYPE_PROMOTE = "11";


}
