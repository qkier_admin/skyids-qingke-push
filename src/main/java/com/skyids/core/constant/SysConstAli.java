package com.skyids.core.constant;

import java.util.HashMap;
import java.util.Map;

public class SysConstAli {

    /**AccessKey ID**/
    public static final String ALI_PLATFORM_ID = "LTAI4Foc1dXCUPz4eqYeKQX1";

    /**AccessKey Secret**/
    public static final String ALI_QINGKE_SIGN = "OWzza1aAV2cmiNeb7g60AiVVazZhQA";

    public final static String ALI_ENCODEING_UTF8 = "UTF-8";   //编码

    /**暴露模板ID与真实模板ID的关系容器**/
    public static final Map<Integer,String> ALI_TPL_MAP = new HashMap<>();

    static {
        //阿里短信模板
        ALI_TPL_MAP.put(8,"SMS_185231894");//验证码${code}，您正在通过手机号码注册账号，如非本人操作，请忽略。
        ALI_TPL_MAP.put(9,"SMS_185246908");//验证码${code}，您正在登录账号，如非本人操作，请忽略。
        ALI_TPL_MAP.put(10,"SMS_185231891");//验证码${code}，您正在重置登录密码，如非本人操作，请忽略。
        ALI_TPL_MAP.put(11,"SMS_185241872");//验证码${code}，您正在绑定手机号，如非本人操作，请忽略。
        ALI_TPL_MAP.put(16,"SMS_193233388");//尊敬的轻客用户,您使用的${washerid}洗衣机已经洗衣完毕, 请尽快取衣哦!
        ALI_TPL_MAP.put(17,"SMS_193233392");//尊敬的轻客用户,您正在使用的${washerid}洗衣机出现故障,请打开轻客APP查看故障原因和解决方案。
        ALI_TPL_MAP.put(18,"SMS_193233397");//尊敬的轻客用户,您锁定的${washerid}洗衣机只剩余一分钟支付时间啦,请尽快支付使用。
        ALI_TPL_MAP.put(20,"SMS_193243434");//尊敬的轻客用户，您使用的${washerid}洗衣机启动失败，洗衣款项已经退款至您的轻币账户，可在下次洗衣时使用，请前往“我的钱包”查看。
        ALI_TPL_MAP.put(21,"SMS_193243438");//尊敬的用户，您的洗衣机押金已退回原支付账户，请注意查收，谢谢。
        ALI_TPL_MAP.put(22,"SMS_193233413");//亲，免费洗衣券已发放到您的轻客账户，请尽快使用。感谢您反馈使用轻客洗衣遇到的问题，谢谢~
        ALI_TPL_MAP.put(25,"SMS_193238409");//您正在使用的${washerid}洗衣机出现故障,请在轻客APP查看故障原因和解决方案,或联系${contact_mobile}解决。
    }
}
